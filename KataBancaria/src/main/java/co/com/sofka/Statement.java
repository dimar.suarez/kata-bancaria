package co.com.sofka;

import java.time.LocalDate;
import java.util.Objects;

public abstract class Statement {
    private final LocalDate date;
    private final Balance balance;

    protected Statement(LocalDate date, Balance balance){
        this.date = Objects.requireNonNull(date);
        this.balance = Objects.requireNonNull(balance);
    }

    public LocalDate date() {
        return date;
    }

    public Balance balance() {
        return balance;
    }
}
