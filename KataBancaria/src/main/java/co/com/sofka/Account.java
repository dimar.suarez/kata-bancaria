package co.com.sofka;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Account {
    private Balance balance;
    private final List<Statement> statements = new ArrayList<>();

    public void deposit(Double amount, LocalDate date) {
        Credit credit = new Credit(amount);
        if(balance == null){
            balance = new Balance(amount);
            statements.add(new CreditStatement(date, balance, credit));
            return;
        }
        updateBalance(amount);
        statements.add(new CreditStatement(date, balance, credit));
    }

    public void withdraw(Double amount, LocalDate date){
        Debit debit = new Debit(amount);
        if (balance == null){
            balance = new Balance(-amount);
            statements.add(new DebitStatement(date, balance, debit));
            return;
        }
        updateBalance(-amount);
        statements.add(new DebitStatement(date, balance, debit));
    }

    private void updateBalance(Double value){
        balance = balance.updateBalance(value);
    }

    public void printStatements() {
        System.out.println("date\t\t\t credit\t\t debit\t\t \tbalance");
        for (Statement statement : statements){
            System.out.println(statement);
        }
    }
}
