package co.com.sofka;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class FormatterDate {
    public static LocalDate localDateOf(String date){
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        return LocalDate.parse(date, dtf);
    }
}
