package co.com.sofka;

import java.util.Objects;

public class Debit {
    private final Double value;

    public Debit(Double value){
        this.value = Objects.requireNonNull(value);
    }

    public Double value(){
        return value;
    }
}
