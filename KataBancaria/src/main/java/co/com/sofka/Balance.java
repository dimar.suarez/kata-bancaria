package co.com.sofka;

import java.util.Objects;

public class Balance {
    private final Double value;

    public Balance(Double value){
        this.value = Objects.requireNonNull(value);
    }

    public Double value(){
        return value;
    }

    public Balance updateBalance(Double value){
        return new Balance(this.value + value);
    }
}
