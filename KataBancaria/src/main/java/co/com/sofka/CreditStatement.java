package co.com.sofka;

import java.time.LocalDate;
import java.util.Objects;

public class CreditStatement extends Statement {
    private final Credit credit;
    public CreditStatement(LocalDate date, Balance balance, Credit credit){
        super(date, balance);
        this.credit = Objects.requireNonNull(credit);
    }

    @Override
    public String toString() {
        return super.date() + "\t\t" + credit.value() + "\t\t\t\t\t\t"  + super.balance().value();
    }
}
