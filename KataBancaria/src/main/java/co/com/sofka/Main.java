package co.com.sofka;

public class Main {
    public static void main(String[] args) {
        Account account = new Account();

        account.deposit(1000.00, FormatterDate.localDateOf("10/01/2021"));
        account.deposit(2000.00, FormatterDate.localDateOf("13/01/2021"));
        account.withdraw(500.00, FormatterDate.localDateOf("14/01/2021"));

        account.printStatements();
    }
}
