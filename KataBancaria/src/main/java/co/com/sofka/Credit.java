package co.com.sofka;

import java.util.Objects;

public class Credit {
    private final Double value;

    public Credit(Double value){
        this.value = Objects.requireNonNull(value);
    }

    public Double value(){
        return value;
    }
}
