package co.com.sofka;

import java.time.LocalDate;
import java.util.Objects;

public class DebitStatement extends Statement {
    private final Debit debit;
    public DebitStatement(LocalDate date, Balance balance, Debit debit){
        super(date, balance);
        this.debit = Objects.requireNonNull(debit);
    }

    @Override
    public String toString() {
        return super.date() + "\t\t\t\t\t" + debit.value() + "\t\t\t" + super.balance().value();
    }
}
